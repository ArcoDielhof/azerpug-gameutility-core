local GlobalAddonName, AGU = ...

if AZP == nil then AZP = {} end
AZP.GU = AGU

AZP.GU.VersionControl = {}
AZP.GU.OnLoad = {}
AZP.GU.OnEvent = {}

local VersionControl = AZP.GU.VersionControl
local OnLoad = AZP.GU.OnLoad
local OnEvent = AZP.GU.OnEvent

local CoreConfig = AGU.CoreConfig

local AZPGUCoreVersion = 44
local dash = " - "
local name = "GameUtility"  .. dash .. "Core"
local nameFull = ("AzerPUG " .. name)
local nameShort = "AZP-GU v" .. AZPGUCoreVersion
local promo = (nameFull .. dash ..  "v" .. AZPGUCoreVersion)
local addonMain = LibStub("AceAddon-3.0"):NewAddon("GameUtility", "AceConsole-3.0")

local OptionsCorePanel
local ModuleStats = AGU.ModuleStats
local addonOutOfDateMessage = true

local GameUtilityAddonFrame
local MainTitleFrame
local VersionControlFrame
local CoreButtonsFrame

local ReloadCheckBox
local ReloadButton

local OpenSettingsButton
local OpenOptionsCheckBox

local GameUtilityLDB = LibStub("LibDataBroker-1.1"):NewDataObject("GameUtility", {
	type = "data source",
	text = "GameUtility",
	icon = "Interface\\Icons\\Inv_darkmoon_eye",
    OnClick = function() addonMain:ShowHideFrame() end
})
local icon = LibStub("LibDBIcon-1.0")

function addonMain:ShowHideFrame()
    if GameUtilityAddonFrame:IsShown() then
        GameUtilityAddonFrame:Hide()
        AGUFrameShown = false
    elseif not GameUtilityAddonFrame:IsShown() then
        GameUtilityAddonFrame:Show()
        AGUFrameShown = true
    end
end

function addonMain:OnInitialize()
    self.db = LibStub("AceDB-3.0"):New("GameUtilityLDB",
    {
        profile =
        {
            minimap =
            {
				hide = false,
			},
		},
	})
	icon:Register("GameUtility", GameUtilityLDB, self.db.profile.minimap)
	self:RegisterChatCommand("GameUtility icon", "MiniMapIconToggle")
end

function addonMain:initializeConfig()
    if AZPGUCoreData == nil then
        AZPGUCoreData = CoreConfig
    end
end

function addonMain:OnLoad(self)
    addonMain:initializeConfig()
    addonMain:CreateOptionsPanels()
    addonMain:CreateMainFrame()
end

function addonMain:CreateOptionsPanels()
    OptionsCorePanel = CreateFrame("FRAME", nil)
    OptionsCorePanel.name = "AzerPUG GameUtility"
    InterfaceOptions_AddCategory(OptionsCorePanel)

    local OptionsCoreTitle = OptionsCorePanel:CreateFontString(nil, "ARTWORK", "GameFontNormalHuge")
    OptionsCoreTitle:SetText(promo)
    OptionsCoreTitle:SetWidth(OptionsCorePanel:GetWidth())
    OptionsCoreTitle:SetHeight(OptionsCorePanel:GetHeight())
    OptionsCoreTitle:SetPoint("TOP", 0, -10)

    local ReloadFrame = CreateFrame("Frame", nil, OptionsCorePanel)
    ReloadFrame:SetSize(500, 50)
    ReloadFrame:SetPoint("TOP", 0, -50)
    ReloadFrame.text = ReloadFrame:CreateFontString("ReloadFrameText", "ARTWORK", "GameFontNormalLarge")
    ReloadFrame.text:SetPoint("LEFT", 20, -1)
    ReloadFrame.text:SetJustifyH("LEFT")
    ReloadFrame.text:SetText("Show/Hide reload button.")

    ReloadCheckBox = CreateFrame("CheckButton", nil, ReloadFrame, "ChatConfigCheckButtonTemplate")
    ReloadCheckBox:SetSize(20, 20)
    ReloadCheckBox:SetPoint("LEFT", 0, 0)
    ReloadCheckBox:SetHitRectInsets(0, 0, 0, 0)
    ReloadCheckBox:SetChecked(CoreConfig["optionsChecked"]["ReloadCheckBox"])
    ReloadCheckBox:SetScript("OnClick",function()
        if AZPGUCoreData["optionsChecked"] == nil then AZPGUCoreData["optionsChecked"] = {} end
        if ReloadCheckBox:GetChecked() == true then
            AZPGUCoreData["optionsChecked"]["ReloadCheckBox"] = true
        else
            AZPGUCoreData["optionsChecked"]["ReloadCheckBox"] = false
        end
    end)

    local OpenOptionsFrame = CreateFrame("Frame", nil, OptionsCorePanel)
    OpenOptionsFrame:SetSize(500, 50)
    OpenOptionsFrame:SetPoint("TOP", 0, -75)
    OpenOptionsFrame.text = OpenOptionsFrame:CreateFontString("OpenOptionsFrameText", "ARTWORK", "GameFontNormalLarge")
    OpenOptionsFrame.text:SetPoint("LEFT", 20, -1)
    OpenOptionsFrame.text:SetJustifyH("LEFT")
    OpenOptionsFrame.text:SetText("Show/Hide options button.")

    OpenOptionsCheckBox = CreateFrame("CheckButton", nil, OpenOptionsFrame, "ChatConfigCheckButtonTemplate")
    OpenOptionsCheckBox:SetSize(20, 20)
    OpenOptionsCheckBox:SetPoint("LEFT", 0, 0)
    OpenOptionsCheckBox:SetHitRectInsets(0, 0, 0, 0)
    OpenOptionsCheckBox:SetChecked(CoreConfig["optionsChecked"]["OpenOptionsCheckBox"])
    OpenOptionsCheckBox:SetScript("OnClick", function()
        if AZPGUCoreData["optionsChecked"] == nil then AZPGUCoreData["optionsChecked"] = {} end
        if OpenOptionsCheckBox:GetChecked() == true then
            AZPGUCoreData["optionsChecked"]["OpenOptionsCheckBox"] = true
        elseif OpenOptionsCheckBox:GetChecked() == false then
            AZPGUCoreData["optionsChecked"]["OpenOptionsCheckBox"] = false
        end
    end)

    local OptionsCoreText = CreateFrame("Frame", "OptionsCoreText", OptionsCorePanel)
    OptionsCoreText:SetSize(500, 500)
    OptionsCoreText:SetPoint("TOP", 0, -125)
    OptionsCoreText.contentText = OptionsCoreText:CreateFontString("OptionsCoreText", "ARTWORK", "GameFontNormalLarge")
    OptionsCoreText.contentText:SetPoint("TOPLEFT")
    OptionsCoreText.contentText:SetJustifyH("LEFT")
    OptionsCoreText.contentText:SetText(
        "AzerPUG Links:\n" ..
        "Website: www.azerpug.com\n" ..
        "Discord: www.azerpug.com/discord\n" ..
        "Twitch: www.twitch.tv/azerpug\n"
    )

    local RepBarsSubPanel = CreateFrame("FRAME", "RepBarsSubPanel", OptionsCorePanel)
    RepBarsSubPanel.name = "RepBars"
    RepBarsSubPanel.parent = OptionsCorePanel.name
    InterfaceOptions_AddCategory(RepBarsSubPanel)

    local RepBarsSubPanelPHTitle = CreateFrame("Frame", "RepBarsSubPanelPHTitle", RepBarsSubPanel)
    RepBarsSubPanelPHTitle:SetSize(500, 500)
    RepBarsSubPanelPHTitle:SetPoint("TOP", 0, -10)
    RepBarsSubPanelPHTitle.contentText = RepBarsSubPanelPHTitle:CreateFontString("RepBarsSubPanelPHTitle", "ARTWORK", "GameFontNormalHuge")
    RepBarsSubPanelPHTitle.contentText:SetPoint("TOP")
    RepBarsSubPanelPHTitle.contentText:SetText("\124cFFFF0000RepBars Module not found!\124r\n")

    local RepBarsSubPanelPHText = CreateFrame("Frame", "RepBarsSubPanelPHText", RepBarsSubPanel)
    RepBarsSubPanelPHText:SetSize(500, 500)
    RepBarsSubPanelPHText:SetPoint("TOP", 0, -50)
    RepBarsSubPanelPHText.contentText = RepBarsSubPanelPHText:CreateFontString("RepBarsSubPanelPHText", "ARTWORK", "GameFontNormalLarge")
    RepBarsSubPanelPHText.contentText:SetPoint("TOPLEFT")
    RepBarsSubPanelPHText.contentText:SetText(
        "RepBars Module provides the option to track several Factions at once.\n" ..
        "The factions can easily be selected within the standard GUI.\n"
    )

    local ChattyThingsSubPanel = CreateFrame("FRAME", "ChattyThingsSubPanel")
    ChattyThingsSubPanel.name = "ChattyThings"
    ChattyThingsSubPanel.parent = OptionsCorePanel.name
    InterfaceOptions_AddCategory(ChattyThingsSubPanel)

    local ChattyThingsSubPanelPHTitle = CreateFrame("Frame", "ChattyThingsSubPanelPHTitle", ChattyThingsSubPanel)
    ChattyThingsSubPanelPHTitle:SetSize(500, 500)
    ChattyThingsSubPanelPHTitle:SetPoint("TOP", 0, -10)
    ChattyThingsSubPanelPHTitle.contentText = ChattyThingsSubPanelPHTitle:CreateFontString("ChattyThingsSubPanelPHTitle", "ARTWORK", "GameFontNormalHuge")
    ChattyThingsSubPanelPHTitle.contentText:SetPoint("TOP")
    ChattyThingsSubPanelPHTitle.contentText:SetText("\124cFFFF0000ChattyThings Module not found!\124r\n")

    local ChattyThingsSubPanelPHText = CreateFrame("Frame", "ChattyThingsSubPanelPHText", ChattyThingsSubPanel)
    ChattyThingsSubPanelPHText:SetSize(500, 500)
    ChattyThingsSubPanelPHText:SetPoint("TOP", 0, -50)
    ChattyThingsSubPanelPHText.contentText = ChattyThingsSubPanelPHText:CreateFontString("ChattyThingsSubPanelPHText", "ARTWORK", "GameFontNormalLarge")
    ChattyThingsSubPanelPHText.contentText:SetPoint("TOPLEFT")
    ChattyThingsSubPanelPHText.contentText:SetText(
        "ChattyThings Module provides enhanced chat options.\n" ..
        "In the options panel, a user defined nickname can be added behind the char name.\n"
    )

    local QuestEfficiencySubPanel = CreateFrame("FRAME", "QuestEfficiencySubPanel")
    QuestEfficiencySubPanel.name = "QuestEfficiency"
    QuestEfficiencySubPanel.parent = OptionsCorePanel.name
    InterfaceOptions_AddCategory(QuestEfficiencySubPanel)

    local QuestEfficiencySubPanelPHTitle = CreateFrame("Frame", "QuestEfficiencySubPanelPHTitle", QuestEfficiencySubPanel)
    QuestEfficiencySubPanelPHTitle:SetSize(500, 500)
    QuestEfficiencySubPanelPHTitle:SetPoint("TOP", 0, -10)
    QuestEfficiencySubPanelPHTitle.contentText = QuestEfficiencySubPanelPHTitle:CreateFontString("QuestEfficiencySubPanelPHTitle", "ARTWORK", "GameFontNormalHuge")
    QuestEfficiencySubPanelPHTitle.contentText:SetPoint("TOP")
    QuestEfficiencySubPanelPHTitle.contentText:SetText("\124cFFFF0000QuestEfficiency Module not found!\124r\n")

    local QuestEfficiencySubPanelPHText = CreateFrame("Frame", "QuestEfficiencySubPanelPHText", QuestEfficiencySubPanel)
    QuestEfficiencySubPanelPHText:SetSize(500, 500)
    QuestEfficiencySubPanelPHText:SetPoint("TOP", 0, -50)
    QuestEfficiencySubPanelPHText.contentText = QuestEfficiencySubPanelPHText:CreateFontString("QuestEfficiencySubPanelPHText", "ARTWORK", "GameFontNormalLarge")
    QuestEfficiencySubPanelPHText.contentText:SetPoint("TOPLEFT")
    QuestEfficiencySubPanelPHText.contentText:SetText(
        "QuestEfficiency Module helps the player efficiently sart, do and end quests.\n" ..
        "Currently: Pre-Selects the most valuable quest reward.\n"
    )

    local LevelStatsSubPanel = CreateFrame("FRAME", "LevelStatsSubPanel")
    LevelStatsSubPanel.name = "LevelStats"
    LevelStatsSubPanel.parent = OptionsCorePanel.name
    InterfaceOptions_AddCategory(LevelStatsSubPanel)

    local LevelStatsSubPanelPHTitle = CreateFrame("Frame", "LevelStatsSubPanelPHTitle", LevelStatsSubPanel)
    LevelStatsSubPanelPHTitle:SetSize(500, 500)
    LevelStatsSubPanelPHTitle:SetPoint("TOP", 0, -10)
    LevelStatsSubPanelPHTitle.contentText = LevelStatsSubPanelPHTitle:CreateFontString("LevelStatsSubPanelPHTitle", "ARTWORK", "GameFontNormalHuge")
    LevelStatsSubPanelPHTitle.contentText:SetPoint("TOP")
    LevelStatsSubPanelPHTitle.contentText:SetText("\124cFFFF0000LevelStats Module not found!\124r\n")

    local LevelStatsSubPanelPHText = CreateFrame("Frame", "LevelStatsSubPanelPHText", LevelStatsSubPanel)
    LevelStatsSubPanelPHText:SetSize(500, 500)
    LevelStatsSubPanelPHText:SetPoint("TOP", 0, -50)
    LevelStatsSubPanelPHText.contentText = LevelStatsSubPanelPHText:CreateFontString("LevelStatsSubPanelPHText", "ARTWORK", "GameFontNormalLarge")
    LevelStatsSubPanelPHText.contentText:SetPoint("TOPLEFT")
    LevelStatsSubPanelPHText.contentText:SetText(
        "LevelStats Module calculates the estimated time until the next level.\n" ..
        "These stats are based on sessionTime and sessionXP."
    )

    local UnLockablesSubPanel = CreateFrame("FRAME", "UnLockablesSubPanel")
    UnLockablesSubPanel.name = "UnLockables"
    UnLockablesSubPanel.parent = OptionsCorePanel.name
    InterfaceOptions_AddCategory(UnLockablesSubPanel)

    local UnLockablesSubPanelPHTitle = CreateFrame("Frame", "UnLockablesSubPanelPHTitle", UnLockablesSubPanel)
    UnLockablesSubPanelPHTitle:SetSize(500, 500)
    UnLockablesSubPanelPHTitle:SetPoint("TOP", 0, -10)
    UnLockablesSubPanelPHTitle.contentText = UnLockablesSubPanelPHTitle:CreateFontString("UnLockablesSubPanelPHTitle", "ARTWORK", "GameFontNormalHuge")
    UnLockablesSubPanelPHTitle.contentText:SetPoint("TOP")
    UnLockablesSubPanelPHTitle.contentText:SetText("\124cFFFF0000UnLockables Module not found!\124r\n")

    local UnLockablesSubPanelPHText = CreateFrame("Frame", "UnLockablesSubPanelPHText", UnLockablesSubPanel)
    UnLockablesSubPanelPHText:SetSize(500, 500)
    UnLockablesSubPanelPHText:SetPoint("TOP", 0, -50)
    UnLockablesSubPanelPHText.contentText = UnLockablesSubPanelPHText:CreateFontString("UnLockablesSubPanelPHText", "ARTWORK", "GameFontNormalLarge")
    UnLockablesSubPanelPHText.contentText:SetPoint("TOPLEFT")
    UnLockablesSubPanelPHText.contentText:SetText(
        "UnLockables Module shows the required quest and/or questlines for certain unlockables."
    )
end

function addonMain:CreateMainFrame()
    GameUtilityAddonFrame = CreateFrame("FRAME", "GameUtilityAddonFrame", UIParent, "BackdropTemplate")
    GameUtilityAddonFrame:SetPoint("TOPLEFT", 0, 0)
    GameUtilityAddonFrame:EnableMouse(true)
    GameUtilityAddonFrame:SetMovable(true)
    GameUtilityAddonFrame:SetResizable(true)
    GameUtilityAddonFrame:RegisterForDrag("LeftButton")
    GameUtilityAddonFrame:SetSize(325, 220)
    GameUtilityAddonFrame:SetMinResize(325, 220)
    GameUtilityAddonFrame:SetScript("OnDragStart", GameUtilityAddonFrame.StartMoving)
    GameUtilityAddonFrame:SetScript("OnDragStop", GameUtilityAddonFrame.StopMovingOrSizing)
    GameUtilityAddonFrame:SetScript("OnEvent", function(...) addonMain:OnEvent(...) end)
    GameUtilityAddonFrame:RegisterEvent("PLAYER_ENTERING_WORLD")
    GameUtilityAddonFrame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
    GameUtilityAddonFrame:RegisterEvent("PLAYER_LOGIN")
    GameUtilityAddonFrame:RegisterEvent("ADDON_LOADED")
    GameUtilityAddonFrame:RegisterEvent("UPDATE_FACTION")

    GameUtilityAddonFrame:SetBackdrop({
        bgFile = "Interface/Tooltips/UI-Tooltip-Background",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        edgeSize = 12,
        insets = { left = 1, right = 1, top = 1, bottom = 1 },
    })
    GameUtilityAddonFrame:SetBackdropColor(0.5, 0.5, 0.5, 0.75)

    MainTitleFrame = CreateFrame("Frame", nil, GameUtilityAddonFrame, "BackdropTemplate")
    MainTitleFrame:SetHeight("20")
    MainTitleFrame:SetWidth(GameUtilityAddonFrame:GetWidth())
    MainTitleFrame:SetBackdrop({
        bgFile = "Interface/Tooltips/UI-Tooltip-Background",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        edgeSize = 12,
        insets = { left = 1, right = 1, top = 1, bottom = 1 },
    })
    MainTitleFrame:SetBackdropColor(0.3, 0.3, 0.3, 1)
    MainTitleFrame:SetPoint("TOPLEFT")
    MainTitleFrame:SetPoint("TOPRIGHT")
    MainTitleFrame.contentText = MainTitleFrame:CreateFontString(nil, "ARTWORK", "GameFontNormalLarge")
    MainTitleFrame.contentText:SetWidth(MainTitleFrame:GetWidth())
    MainTitleFrame.contentText:SetHeight(MainTitleFrame:GetHeight())
    MainTitleFrame.contentText:SetPoint("CENTER", 0, -1)
    MainTitleFrame.contentText:SetText("\124cFF00FFFF" .. promo .. "\124r")

    ModuleStats["Tabs"]["Core"] = CreateFrame("Button", nil, GameUtilityAddonFrame, "BackdropTemplate")
    ModuleStats["Tabs"]["Core"].contentText = ModuleStats["Tabs"]["Core"]:CreateFontString(nil, "ARTWORK", "GameFontNormal")
    ModuleStats["Tabs"]["Core"].contentText:SetText("CORE")
    ModuleStats["Tabs"]["Core"].contentText:SetTextColor(0.75, 0.75, 0.75, 1)
    ModuleStats["Tabs"]["Core"]:SetWidth("40")
    ModuleStats["Tabs"]["Core"]:SetHeight("20")
    ModuleStats["Tabs"]["Core"].contentText:SetWidth(ModuleStats["Tabs"]["Core"]:GetWidth())
    ModuleStats["Tabs"]["Core"].contentText:SetHeight(ModuleStats["Tabs"]["Core"]:GetHeight())
    ModuleStats["Tabs"]["Core"]:SetPoint("TOPLEFT", MainTitleFrame, "BOTTOMLEFT", 2, 2);
    ModuleStats["Tabs"]["Core"].contentText:SetPoint("CENTER", 0, -1)
    ModuleStats["Tabs"]["Core"]:SetBackdrop({
        bgFile = "Interface/Tooltips/UI-Tooltip-Background",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        edgeSize = 8,
        insets = { left = 1, right = 1, top = 1, bottom = 1 },
    })
    ModuleStats["Tabs"]["Core"]:SetBackdropColor(0.75, 0.75, 0.75, 1)
    ModuleStats["Tabs"]["Core"]:SetScript("OnClick", function() addonMain:ShowHideSubFrames(ModuleStats["Frames"]["Core"]) end )

    ModuleStats["Tabs"]["RepBars"] = CreateFrame("Button", nil, GameUtilityAddonFrame, "BackdropTemplate")
    ModuleStats["Tabs"]["RepBars"]:SetSize(1, 1)
    ModuleStats["Tabs"]["RepBars"]:SetPoint("LEFT", ModuleStats["Tabs"]["Core"], "RIGHT", 0, 0);

    ModuleStats["Tabs"]["ChattyThings"] = CreateFrame("Button", nil, GameUtilityAddonFrame, "BackdropTemplate")
    ModuleStats["Tabs"]["ChattyThings"]:SetSize(1, 1)
    ModuleStats["Tabs"]["ChattyThings"]:SetPoint("LEFT", ModuleStats["Tabs"]["RepBars"], "RIGHT", 0, 0);

    ModuleStats["Tabs"]["QuestEfficiency"] = CreateFrame("Button", nil, GameUtilityAddonFrame, "BackdropTemplate")
    ModuleStats["Tabs"]["QuestEfficiency"]:SetSize(1, 1)
    ModuleStats["Tabs"]["QuestEfficiency"]:SetPoint("LEFT", ModuleStats["Tabs"]["ChattyThings"], "RIGHT", 0, 0);

    ModuleStats["Tabs"]["LevelStats"] = CreateFrame("Button", nil, GameUtilityAddonFrame, "BackdropTemplate")
    ModuleStats["Tabs"]["LevelStats"]:SetSize(1, 1)
    ModuleStats["Tabs"]["LevelStats"]:SetPoint("LEFT", ModuleStats["Tabs"]["QuestEfficiency"], "RIGHT", 0, 0);

    ModuleStats["Tabs"]["UnLockables"] = CreateFrame("Button", nil, GameUtilityAddonFrame, "BackdropTemplate")
    ModuleStats["Tabs"]["UnLockables"]:SetSize(1, 1)
    ModuleStats["Tabs"]["UnLockables"]:SetPoint("LEFT", ModuleStats["Tabs"]["LevelStats"], "RIGHT", 0, 0);

    local GUAddonFrameCloseButton = CreateFrame("Button", "GUAddonFrameCloseButton", GameUtilityAddonFrame, "UIPanelCloseButton")
    GUAddonFrameCloseButton:SetWidth(MainTitleFrame:GetHeight() + 3)
    GUAddonFrameCloseButton:SetHeight(MainTitleFrame:GetHeight() + 4)
    GUAddonFrameCloseButton:SetPoint("TOPRIGHT", MainTitleFrame, "TOPRIGHT", 2, 2)
    GUAddonFrameCloseButton:SetScript("OnClick", function() addonMain:ShowHideFrame() end )

    local GUAddonFrameResizeButton = CreateFrame("Frame", "GUAddonFrameResizeButton", GameUtilityAddonFrame, "BackdropTemplate")
    GUAddonFrameResizeButton:SetWidth(20)
    GUAddonFrameResizeButton:SetHeight(20)
    GUAddonFrameResizeButton:SetPoint("BOTTOMRIGHT")
    GUAddonFrameResizeButton:SetBackdrop({
        bgFile = "Interface/Tooltips/UI-Tooltip-Background",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        edgeSize = 8,
        insets = { left = 1, right = 1, top = 1, bottom = 1 },
    })
    GUAddonFrameResizeButton:SetBackdropColor(0.75, 0.75, 0.75, 1)
    GUAddonFrameResizeButton:SetScript("OnMouseDown", function() GameUtilityAddonFrame:StartSizing("BOTTOMRIGHT") end)
    GUAddonFrameResizeButton:SetScript("OnMouseUp", function() GameUtilityAddonFrame:StopMovingOrSizing() end)
    GUAddonFrameResizeButton:RegisterForDrag("LeftButton")
    GUAddonFrameResizeButton:EnableMouse(true)

    addonMain:CreateSubFrames()
end

function addonMain:AddMainFrameTabButton(tabName)
    local CurrentTab
    if tabName == "RB" then
        CurrentTab = ModuleStats["Tabs"]["RepBars"]
        CurrentTab:SetWidth("20")
        CurrentTab:SetHeight("20")
        CurrentTab.contentText = CurrentTab:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
        CurrentTab.contentText:SetText("RB")
        CurrentTab.contentText:SetWidth(CurrentTab:GetWidth())
        CurrentTab.contentText:SetHeight(CurrentTab:GetHeight())
        CurrentTab.contentText:SetPoint("CENTER", 0, 0)
        CurrentTab:SetBackdrop({
            bgFile = "Interface/Tooltips/UI-Tooltip-Background",
            edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
            edgeSize = 8,
            insets = { left = 1, right = 1, top = 1, bottom = 1 },
        })
        CurrentTab:SetScript("OnClick", function() addonMain:ShowHideSubFrames(ModuleStats["Frames"]["RepBars"]) end )
        if ModuleStats["Frames"]["RepBars"] ~= nil then
            CurrentTab:SetBackdropColor(ModuleStats["Tabs"]["Core"]:GetBackdropColor())
            CurrentTab.contentText:SetTextColor(ModuleStats["Tabs"]["Core"].contentText:GetTextColor())
        else
            CurrentTab:SetBackdropColor(0.25, 0.25, 0.25, 0.75)
            CurrentTab.contentText:SetTextColor(0.5, 0.5, 0.5, 0.75)
        end
    elseif tabName == "CT" then
        CurrentTab = ModuleStats["Tabs"]["ChattyThings"]
        CurrentTab:SetWidth("20")
        CurrentTab:SetHeight("20")
        CurrentTab.contentText = CurrentTab:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
        CurrentTab.contentText:SetText("CT")
        CurrentTab.contentText:SetWidth(CurrentTab:GetWidth())
        CurrentTab.contentText:SetHeight(CurrentTab:GetHeight())
        CurrentTab.contentText:SetPoint("CENTER", 0, 0)
        CurrentTab:SetBackdrop({
            bgFile = "Interface/Tooltips/UI-Tooltip-Background",
            edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
            edgeSize = 8,
            insets = { left = 1, right = 1, top = 1, bottom = 1 },
        })
        --CurrentTab:SetScript("OnClick", function() addonMain:ShowHideSubFrames(ModuleStats["Frames"]["ChattyThings"]) end )
        if ModuleStats["Frames"]["ChattyThings"] ~= nil then
            CurrentTab:SetBackdropColor(ModuleStats["Tabs"]["Core"]:GetBackdropColor())
            CurrentTab.contentText:SetTextColor(ModuleStats["Tabs"]["Core"].contentText:GetTextColor())
        else
            CurrentTab:SetBackdropColor(0.25, 0.25, 0.25, 0.75)
            CurrentTab.contentText:SetTextColor(0.5, 0.5, 0.5, 0.75)
        end
    elseif tabName == "QE" then
        CurrentTab = ModuleStats["Tabs"]["QuestEfficiency"]
        CurrentTab:SetWidth("20")
        CurrentTab:SetHeight("20")
        CurrentTab.contentText = CurrentTab:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
        CurrentTab.contentText:SetText("QE")
        CurrentTab.contentText:SetWidth(CurrentTab:GetWidth())
        CurrentTab.contentText:SetHeight(CurrentTab:GetHeight())
        CurrentTab.contentText:SetPoint("CENTER", 0, 0)
        CurrentTab:SetBackdrop({
            bgFile = "Interface/Tooltips/UI-Tooltip-Background",
            edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
            edgeSize = 8,
            insets = { left = 1, right = 1, top = 1, bottom = 1 },
        })
        --CurrentTab:SetScript("OnClick", function() addonMain:ShowHideSubFrames(ModuleStats["Frames"]["QuestEfficiency"]) end )
        if ModuleStats["Frames"]["QuestEfficiency"] ~= nil then
            CurrentTab:SetBackdropColor(ModuleStats["Tabs"]["Core"]:GetBackdropColor())
            CurrentTab.contentText:SetTextColor(ModuleStats["Tabs"]["Core"].contentText:GetTextColor())
        else
            CurrentTab:SetBackdropColor(0.25, 0.25, 0.25, 0.75)
            CurrentTab.contentText:SetTextColor(0.5, 0.5, 0.5, 0.75)
        end
    elseif tabName == "LS" then
        CurrentTab = ModuleStats["Tabs"]["LevelStats"]
        CurrentTab:SetWidth("20")
        CurrentTab:SetHeight("20")
        CurrentTab.contentText = CurrentTab:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
        CurrentTab.contentText:SetText("LS")
        CurrentTab.contentText:SetWidth(CurrentTab:GetWidth())
        CurrentTab.contentText:SetHeight(CurrentTab:GetHeight())
        CurrentTab.contentText:SetPoint("CENTER", 0, 0)
        CurrentTab:SetBackdrop({
            bgFile = "Interface/Tooltips/UI-Tooltip-Background",
            edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
            edgeSize = 8,
            insets = { left = 1, right = 1, top = 1, bottom = 1 },
        })
        CurrentTab:SetScript("OnClick", function() addonMain:ShowHideSubFrames(ModuleStats["Frames"]["LevelStats"]) end )
        if ModuleStats["Frames"]["LevelStats"] ~= nil then
            CurrentTab:SetBackdropColor(ModuleStats["Tabs"]["Core"]:GetBackdropColor())
            CurrentTab.contentText:SetTextColor(ModuleStats["Tabs"]["Core"].contentText:GetTextColor())
        else
            CurrentTab:SetBackdropColor(0.25, 0.25, 0.25, 0.75)
            CurrentTab.contentText:SetTextColor(0.5, 0.5, 0.5, 0.75)
        end
    elseif tabName == "UL" then
        CurrentTab = ModuleStats["Tabs"]["UnLockables"]
        CurrentTab:SetWidth("20")
        CurrentTab:SetHeight("20")
        CurrentTab.contentText = CurrentTab:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
        CurrentTab.contentText:SetText("UL")
        CurrentTab.contentText:SetWidth(CurrentTab:GetWidth())
        CurrentTab.contentText:SetHeight(CurrentTab:GetHeight())
        CurrentTab.contentText:SetPoint("CENTER", 0, 0)
        CurrentTab:SetBackdrop({
            bgFile = "Interface/Tooltips/UI-Tooltip-Background",
            edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
            edgeSize = 8,
            insets = { left = 1, right = 1, top = 1, bottom = 1 },
        })
        CurrentTab:SetScript("OnClick", function() addonMain:ShowHideSubFrames(ModuleStats["Frames"]["UnLockables"]) end )
        if ModuleStats["Frames"]["UnLockables"] ~= nil then
            CurrentTab:SetBackdropColor(ModuleStats["Tabs"]["Core"]:GetBackdropColor())
            CurrentTab.contentText:SetTextColor(ModuleStats["Tabs"]["Core"].contentText:GetTextColor())
        else
            CurrentTab:SetBackdropColor(0.25, 0.25, 0.25, 0.75)
            CurrentTab.contentText:SetTextColor(0.5, 0.5, 0.5, 0.75)
        end
    end
end

function addonMain:CreateSubFrames()
    ModuleStats["Frames"]["Core"] = CreateFrame("FRAME", nil, GameUtilityAddonFrame, "BackdropTemplate")
    ModuleStats["Frames"]["Core"]:SetPoint("TOPLEFT", 0, -36)
    ModuleStats["Frames"]["Core"]:SetPoint("BOTTOMRIGHT")
    ModuleStats["Frames"]["Core"]:SetBackdrop({
        bgFile = "Interface/Tooltips/UI-Tooltip-Background",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        edgeSize = 12,
        insets = { left = 1, right = 1, top = 1, bottom = 1 },
    })
    ModuleStats["Frames"]["Core"]:SetBackdropColor(0.75, 0.75, 0.75, 0.5)

    ModuleStats["Frames"]["RepBars"] = CreateFrame("FRAME", nil, GameUtilityAddonFrame, "BackdropTemplate")
    ModuleStats["Frames"]["RepBars"]:SetPoint("TOPLEFT", 0, -36)
    ModuleStats["Frames"]["RepBars"]:SetPoint("BOTTOMRIGHT")
    ModuleStats["Frames"]["RepBars"]:SetBackdrop({
        bgFile = "Interface/Tooltips/UI-Tooltip-Background",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        edgeSize = 12,
        insets = { left = 1, right = 1, top = 1, bottom = 1 },
    })
    ModuleStats["Frames"]["RepBars"]:SetBackdropColor(0.75, 0.75, 0.75, 0.5)

    ModuleStats["Frames"]["LevelStats"] = CreateFrame("FRAME", nil, GameUtilityAddonFrame, "BackdropTemplate")
    ModuleStats["Frames"]["LevelStats"]:SetPoint("TOPLEFT", 0, -36)
    ModuleStats["Frames"]["LevelStats"]:SetPoint("BOTTOMRIGHT")
    ModuleStats["Frames"]["LevelStats"]:SetBackdrop({
        bgFile = "Interface/Tooltips/UI-Tooltip-Background",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        edgeSize = 12,
        insets = { left = 1, right = 1, top = 1, bottom = 1 },
    })
    ModuleStats["Frames"]["LevelStats"]:SetBackdropColor(0.75, 0.75, 0.75, 0.5)

    ModuleStats["Frames"]["UnLockables"] = CreateFrame("FRAME", nil, GameUtilityAddonFrame, "BackdropTemplate")
    ModuleStats["Frames"]["UnLockables"]:SetPoint("TOPLEFT", 0, -36)
    ModuleStats["Frames"]["UnLockables"]:SetPoint("BOTTOMRIGHT")
    ModuleStats["Frames"]["UnLockables"]:SetBackdrop({
        bgFile = "Interface/Tooltips/UI-Tooltip-Background",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        edgeSize = 12,
        insets = { left = 1, right = 1, top = 1, bottom = 1 },
    })
    ModuleStats["Frames"]["UnLockables"]:SetBackdropColor(0.75, 0.75, 0.75, 0.5)

    addonMain:CoreSubFrame()
end

function addonMain:CoreSubFrame()
    CoreButtonsFrame = CreateFrame("FRAME", nil, ModuleStats["Frames"]["Core"])
    CoreButtonsFrame:SetSize(100, 50)
    CoreButtonsFrame:SetPoint("TOPLEFT")

    ReloadButton = CreateFrame("Button", nil, CoreButtonsFrame, "UIPanelButtonTemplate")
    ReloadButton:SetSize(1, 1)
    ReloadButton:SetPoint("TOPLEFT", 5, -5)
    ReloadButton:Hide()

    OpenSettingsButton = CreateFrame("Button", nil, CoreButtonsFrame, "UIPanelButtonTemplate")
    OpenSettingsButton:SetSize(1, 1)
    OpenSettingsButton:SetPoint("TOPLEFT", ReloadButton, "BOTTOMLEFT", 0, 0);
    OpenSettingsButton:Hide()

    VersionControlFrame = CreateFrame("FRAME", nil, ModuleStats["Frames"]["Core"])
    VersionControlFrame.contentText = VersionControlFrame:CreateFontString(nil, "ARTWORK", "GameFontNormalLarge")
    VersionControlFrame:SetSize(200, 100)
    VersionControlFrame:SetPoint("TOPRIGHT")
    VersionControlFrame.contentText:SetText("\124cFFFFFF00All Modules Updated!\124r")
    VersionControlFrame.contentText:SetPoint("TOP", 0, -5)

    ModuleStats["Frames"]["Core"]:SetWidth(CoreButtonsFrame:GetWidth() + 10 + VersionControlFrame:GetWidth())
    ModuleStats["Frames"]["Core"]:SetHeight(math.max(CoreButtonsFrame:GetWidth(), VersionControlFrame:GetHeight()))
end

function addonMain:ShowHideSubFrames(ShowFrame)
    ModuleStats["Frames"]["Core"]:Hide()
    ModuleStats["Frames"]["RepBars"]:Hide()
    --ModuleStats["Frames"]["QuestEfficiency"]:Hide()
    ModuleStats["Frames"]["LevelStats"]:Hide()
    ModuleStats["Frames"]["UnLockables"]:Hide()

    ShowFrame:Show()
    GameUtilityAddonFrame:SetSize(ShowFrame:GetWidth(), ShowFrame:GetHeight() + 36)
    MainTitleFrame:SetWidth(GameUtilityAddonFrame:GetWidth())
end

function addonMain:CoreVersionControl()
    if addonOutOfDateMessage == true then
        local mainText = "\124cFF00FFFFAzerPUG-GameUtility\nOut of date modules:\124r"
        local tempText = ""
        local RepBarsVersion
        local ChattyThingsVersion
        local QuestEfficiencyVersion
        local LevelStatsVersion
        local UnLockablesVersion
        local coreVersionUpdated = true

        if IsAddOnLoaded("AzerPUG-GameUtility-RepBars") then
            RepBarsVersion = VersionControl:RepBars()
            if RepBarsVersion < ModuleStats["Versions"]["RepBars"] then
                tempText = tempText .. "\n\124cFFFF0000RepBars\124r"
            elseif RepBarsVersion > ModuleStats["Versions"]["RepBars"] then
                coreVersionUpdated = false
            end
        end

        if IsAddOnLoaded("AzerPUG-GameUtility-ChattyThings") then
            ChattyThingsVersion = VersionControl:ChattyThings()
            if ChattyThingsVersion < ModuleStats["Versions"]["ChattyThings"] then
                tempText = tempText .. "\n\124cFFFF0000ChattyThings\124r"
            elseif ChattyThingsVersion > ModuleStats["Versions"]["ChattyThings"] then
                coreVersionUpdated = false
            end
        end

        if IsAddOnLoaded("AzerPUG-GameUtility-QuestEfficiency") then
            QuestEfficiencyVersion = VersionControl:QuestEfficiency()
            if QuestEfficiencyVersion < ModuleStats["Versions"]["QuestEfficiency"] then
                tempText = tempText .. "\n\124cFFFF0000QuestEfficiency\124r"
            elseif QuestEfficiencyVersion > ModuleStats["Versions"]["QuestEfficiency"] then
                coreVersionUpdated = false
            end
        end

        if IsAddOnLoaded("AzerPUG-GameUtility-LevelStats") then
            LevelStatsVersion = VersionControl:LevelStats()
            if LevelStatsVersion < ModuleStats["Versions"]["LevelStats"] then
                tempText = tempText .. "\n\124cFFFF0000LevelStats\124r"
            elseif LevelStatsVersion > ModuleStats["Versions"]["LevelStats"] then
                coreVersionUpdated = false
            end
        end

        if IsAddOnLoaded("AzerPUG-GameUtility-UnLockables") then
            UnLockablesVersion = VersionControl:UnLockables()
            if UnLockablesVersion < ModuleStats["Versions"]["UnLockables"] then
                tempText = tempText .. "\n\124cFFFF0000UnLockables\124r"
            elseif UnLockablesVersion > ModuleStats["Versions"]["UnLockables"] then
                coreVersionUpdated = false
            end
        end

        if coreVersionUpdated == false then
            tempText = tempText .. "\n\124cFFFF0000Core\124r"
        end

        if #tempText == 0 then
            tempText = "\n\124cFF00FF00None!\124r"
        end

        addonOutOfDateMessage = false
        VersionControlFrame.contentText:SetText(mainText .. "\n" .. tempText)
    end
end

function addonMain:OnLoadedSelf()
    if AZPGUCoreData["optionsChecked"] == nil then
        AZPGUCoreData["optionsChecked"] = {}
    end

    if AZPGUCoreData["optionsChecked"]["ReloadCheckBox"] then
        ReloadCheckBox:SetChecked(true)
        ReloadButton.contentText = ReloadButton:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
        ReloadButton.contentText:SetText("Reload!")
        ReloadButton:SetSize(100, 25)
        ReloadButton.contentText:SetSize(ReloadButton:GetWidth(), 15)
        ReloadButton.contentText:SetPoint("CENTER", 0, -1)
        ReloadButton:Show()
        ReloadButton:SetScript("OnClick", function() ReloadUI(); end )
    end

    if (AZPGUCoreData["optionsChecked"]["OpenOptionsCheckBox"]) then
        OpenOptionsCheckBox:SetChecked(true)
        OpenSettingsButton.contentText = OpenSettingsButton:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
        OpenSettingsButton.contentText:SetText("Open Options!")
        OpenSettingsButton:SetSize(100, 25)
        OpenSettingsButton.contentText:SetSize(OpenSettingsButton:GetWidth(), 15)
        OpenSettingsButton.contentText:SetPoint("CENTER", 0, -1)
        OpenSettingsButton:Show()
        OpenSettingsButton:SetScript("OnClick", function() InterfaceOptionsFrame_OpenToCategory(OptionsCorePanel); InterfaceOptionsFrame_OpenToCategory(OptionsCorePanel); end )
    end
end

function addonMain:OnEvent(self, event, ...)  -- Removed self, before the event var.
    if event == "PLAYER_ENTERING_WORLD" then
        addonMain:CoreVersionControl()
        addonMain:ShowHideSubFrames(ModuleStats["Frames"]["Core"])
        if AGUFrameShown == false then GameUtilityAddonFrame:Hide() end
    elseif event == "ADDON_LOADED" then
        local addonName = ...
        if addonName == "AzerPUG-GameUtility-Core" then
            addonMain:OnLoadedSelf()
        elseif addonName == "AzerPUG-GameUtility-RepBars" then
            addonMain:AddMainFrameTabButton("RB")
            OnLoad:RepBars()
        elseif addonName == "AzerPUG-GameUtility-ChattyThings" then
            addonMain:AddMainFrameTabButton("CT")
            OnLoad:ChattyThings()
        elseif addonName == "AzerPUG-GameUtility-QuestEfficiency" then
            addonMain:AddMainFrameTabButton("QE")
            OnLoad:QuestEfficiency()
        elseif addonName == "AzerPUG-GameUtility-LevelStats" then
            addonMain:AddMainFrameTabButton("LS")
            OnLoad:LevelStats()
        elseif addonName == "AzerPUG-GameUtility-UnLockables" then
            addonMain:AddMainFrameTabButton("UL")
            OnLoad:UnLockables()
        elseif addonName == "AzerPUG-GameUtility-VendorStuff" then
            --addonMain:AddMainFrameTabButton("VS")
            OnLoad:VendorStuff()
        end
    end

    if event ~= "ADDON_LOADED" then
        if IsAddOnLoaded("AzerPUG-GameUtility-RepBars") then
            OnEvent:RepBars(event, ...)
        end
        if IsAddOnLoaded("AzerPUG-GameUtility-ChattyThings") then
            OnEvent:ChattyThings(event, ...)
        end
        if IsAddOnLoaded("AzerPUG-GameUtility-QuestEfficiency") then
            OnEvent:QuestEfficiency(event, ...)
        end
        if IsAddOnLoaded("AzerPUG-GameUtility-LevelStats") then
            OnEvent:LevelStats(event, ...)
        end
        if IsAddOnLoaded("AzerPUG-GameUtility-UnLockables") then
            OnEvent:UnLockables(event, ...)
        end
        if IsAddOnLoaded("AzerPUG-GameUtility-VendorStuff") then
            OnEvent:VendorStuff(event, ...)
        end
    end
end

addonMain:OnLoad()