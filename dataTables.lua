local GlobalAddonName, AGU = ...
_G[GlobalAddonName] = AGU

AGU.ModuleStats =
{
    ["Versions"] =
    {
        ["RepBars"] = 13,
        ["ChattyThings"] = 23,
        ["QuestEfficiency"] = 9,
        ["LevelStats"] = 9,
        ["UnLockables"] = 5,
    },
    ["Tabs"] =
    {
        ["Core"] = nil,
        ["RepBars"] = nil,
        ["ChattyThings"] = nil,
        ["QuestEfficiency"] = nil,
        ["LevelStats"] = nil,
        ["UnLockables"] = nil,
    },
    ["Frames"] =
    {
        ["Core"] = nil,
        ["RepBars"] = nil,
        ["ChattyThings"] = nil,
        ["QuestEfficiency"] = nil,
        ["LevelStats"] = nil,
        ["UnLockables"] = nil,
    },
}

AGU.CoreConfig =
{
    ["optionsChecked"] = {}
}