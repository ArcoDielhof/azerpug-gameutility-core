## Interface: 90005
## Title: |cFF00FFFFAZP|r |cFFFF00FFGameUtility|r |cFFFFFF00Core|r |cFFFF0000(REQUIRED!)|r
## Author: Tex & AzerPUG Gaming Community (www.azerpug.com/discord)
## Notes: Utility for the Game!
## Version: SL 9.0.5 (For actual addon version, check main.lua)
## SavedVariables: GameUtilityLDB
## SavedVariablesPerCharacter: AZPGUCoreData AGUFrameShown

embeds.xml

dataTables.lua
helperFunctions.lua
main.lua